﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Career.Data
{
    public interface IRepository<T> : IDisposable where T : class
    {
        DateTime GetServerDate();
        T Find(object id);
        IQueryable<T> Get();
        IQueryable<T> Get(Expression<Func<T, bool>> predicate);
        bool Any(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(object id);
        void Update(T entity);
    }
}
