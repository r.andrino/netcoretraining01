﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }
        int SaveChanges();
    }
}
