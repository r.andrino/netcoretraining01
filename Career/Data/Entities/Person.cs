﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data.Entities
{
    public class Person
    {
        [StringLength(50), Required]
        public string Firstname { get; set; }
        [StringLength(50), Required]
        public string Lastname { get; set; }
        [StringLength(50)]
        public string Middlename { get; set; }
        public DateTime DateOfBirth { get; set; }
        [StringLength(50)]
        public string Religion { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        public string EmailAddress { get; set; }

    }
}
