﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data.Entities
{
    public class Application
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApplicationID { get; set; }
        [ForeignKey("Applicant")]
        public int ApplicantID { get; set; }
        public Applicant Applicant { get; set; }
        [ForeignKey("OpenPosition")]
        public string PositionCode { get; set; }
        public OpenPosition OpenPosition { get; set; }       
        public DateTime ApplicationDate { get; set; }
    }
}
