﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data.Entities
{
    public class Applicant : Person
    {
        public Applicant()
        {
            Applications = new HashSet<Application>();
        }
        public int ApplicantID { get; set; }
        public DateTime RegistredDate { get; set; }

        public ICollection<Application> Applications { get; set; }
    }
}
