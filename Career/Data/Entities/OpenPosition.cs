﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data.Entities
{
    public class OpenPosition
    {
        public OpenPosition()
        {
            Applications = new HashSet<Application>();
        }
        [Key,StringLength(10),Column(TypeName = "char(10)")]
        public string PositionCode { get; set; }
        [StringLength(50),Required,Column(TypeName = "varchar(50)")]
        public string Title { get; set; }
        [Required]
        public string Responsibilities { get; set; }
        public short TargetApplicants { get; set; }

        public ICollection<Application> Applications { get; set; }
    }
}
