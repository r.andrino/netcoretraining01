﻿using Career.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Data
{
    public class CareerDBContext : DbContext
    {

        public CareerDBContext(DbContextOptions<CareerDBContext> options) : base(options)
        {
            
        }

        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<OpenPosition> OpenPositions { get; set; }
    }
}
