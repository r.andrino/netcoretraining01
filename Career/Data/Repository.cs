﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Career.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;
        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(T entity)
        {
            _unitOfWork.Context.Set<T>().Add(entity);
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return _unitOfWork.Context.Set<T>().Where(predicate).Any();
        }

        public void Delete(object id)
        {
            T existing = _unitOfWork.Context.Set<T>().Find(id);
            _unitOfWork.Context.Set<T>().Remove(existing);
        }

        public void Dispose()
        {
            _unitOfWork.Context.Dispose();
        }

        public T Find(object id)
        {
            return _unitOfWork.Context.Set<T>().Find(id);
        }
        public IQueryable<T> Get()
        {
            return _unitOfWork.Context.Set<T>().AsQueryable<T>();
        }

        public IQueryable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return _unitOfWork.Context.Set<T>().Where(predicate).AsQueryable<T>();
        }

        public DateTime GetServerDate()
        {
            DateTime dateTime = DateTime.Now;
            var conn = _unitOfWork.Context.Database.GetDbConnection();
            try
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    string query = "SELECT GETDATE()";
                    command.CommandText = query;
                    DbDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            dateTime = reader.GetDateTime(0);
                        }
                    }
                    reader.Dispose();
                }
            }
            finally
            {
                conn.Close();
            }
            return dateTime;
        }

        public void Update(T entity)
        {
            _unitOfWork.Context.Set<T>().Attach(entity);
            _unitOfWork.Context.Entry(entity).State = EntityState.Modified;         
        }
    }
}
