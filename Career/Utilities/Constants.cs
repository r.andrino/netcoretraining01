﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Utilities
{
    public static partial class Constants
    {
        public static string ApplicantNotFound = "Applicant was not found!";
        public static string PositionNotFound = "Applicant was not found!";
        public static string UnexpectedFormError = "There's something wrong with the form, please try again later.";
    }
}
