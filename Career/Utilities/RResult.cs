﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Utilities
{
    public class RResult
    {
        public RResult()
        {

        }
        public RResult(Exception exception)
        {
            Succeeded = false;
            Message = exception.Message;
        }
        public RResult(bool succeeded, string message)
        {
            Succeeded = succeeded;
            Message = message;
        }
        public RResult(bool succeeded, string[] messages)
        {
            Succeeded = succeeded;
            Messages = messages;
        }
        public string AlertResult()
        {
            if (Succeeded)
            {
                return "alert alert-success";
            }
            return "alert alert-danger";
        }
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string[] Messages { get; set; }
    }
}
