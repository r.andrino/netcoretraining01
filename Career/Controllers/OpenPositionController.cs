﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Career.Data.Entities;
using Career.Models.ViewModels;
using Career.Services;
using Career.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Career.Controllers
{
    //[Authorize]
    public class OpenPositionController : Controller
    {
        private readonly OpenPositionService openPositionService;
        [TempData]
        public string ResultMessage { get; set; }
        [TempData]
        public string RResult { get; set; }
        public OpenPositionController(OpenPositionService openPositionService)
        {
            this.openPositionService = openPositionService;
        }
        public IActionResult Index(int? applicantID)
        {
            return View(openPositionService.OpenPositions());
        }
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(OpenPositionVM openPositionVM)
        {
            if (ModelState.IsValid)
            {
                var applicant = new OpenPosition() 
                { 
                    PositionCode = openPositionVM.PositionCode.ToUpper(),
                    Title = openPositionVM.Title,
                    Responsibilities = openPositionVM.Responsibilities,
                    TargetApplicants = openPositionVM.TargetApplicants
                };

                var result = openPositionService.AddOpenPosition(applicant);
                RResult = JsonConvert.SerializeObject(result);

                if (result.Succeeded)
                {                    
                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            return View(openPositionVM);
        }
        public IActionResult Edit(string id)
        {
            var applicant = openPositionService.OpenPosition(id);
            if(applicant != null)
            {
                return View(new OpenPositionVM
                {
                    PositionCode = applicant.PositionCode,
                    Title = applicant.Title,
                    Responsibilities = applicant.Responsibilities,
                    TargetApplicants = applicant.TargetApplicants
                });
            }
            ResultMessage = Constants.PositionNotFound;
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public IActionResult Edit(OpenPositionVM openPositionVM)
        {
            if (ModelState.IsValid)
            {
                var openPosition = new OpenPosition()
                {
                    PositionCode = openPositionVM.PositionCode,
                    Title = openPositionVM.Title,
                    Responsibilities = openPositionVM.Responsibilities,
                    TargetApplicants = openPositionVM.TargetApplicants
                };

                var result = openPositionService.Update(openPosition);
                RResult = JsonConvert.SerializeObject(result);

                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(openPositionVM);
        }
        public IActionResult Details(string id)
        {
            var openPosition = openPositionService.OpenPosition(id);
            if (openPosition != null)
            {
                return View(new OpenPositionVM
                {
                    PositionCode = openPosition.PositionCode,
                    Title = openPosition.Title,
                    Responsibilities = openPosition.Responsibilities,
                    TargetApplicants = openPosition.TargetApplicants
                });
            }
            ResultMessage = Constants.PositionNotFound;
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public IActionResult Delete(string positionCode)
        {
            if(positionCode != null)
            {
               var result = openPositionService.Delete(positionCode);
                RResult = JsonConvert.SerializeObject(result);

                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
                return RedirectToAction(nameof(Details),new { id = positionCode });
            }
            ResultMessage = Constants.PositionNotFound;
            return RedirectToAction(nameof(Index));
        }
    }
}