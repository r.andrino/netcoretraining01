﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Career.Data.Entities;
using Career.Models.ViewModels;
using Career.Services;
using Career.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Career.Controllers
{
    [Authorize]
    public class ApplicationController : Controller
    {
        private readonly ApplicantService applicantService;
        [TempData]
        public string ResultMessage { get; set; }
        [TempData]
        public string RResult { get; set; }
        public ApplicationController(ApplicantService applicantService)
        {
            this.applicantService = applicantService;
        }
        public IActionResult Index()
        {
            return View(applicantService.Applicants());
        }
        public IActionResult Apply()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Apply(ApplicantVM applicantVM)
        {
            if (ModelState.IsValid)
            {
                var applicant = new Applicant() 
                { 
                    Firstname = applicantVM.Firstname?.ToUpper(),
                    Lastname = applicantVM.Lastname?.ToUpper(),
                    Middlename = applicantVM.Middlename?.ToUpper(),
                    DateOfBirth = applicantVM.DateOfBirth,
                    Religion = applicantVM.Religion,
                    Nationality = applicantVM.Nationality,
                };

                var result = applicantService.AddApplicant(applicant);
                RResult = JsonConvert.SerializeObject(result);

                if (result.Succeeded)
                {                    
                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            return View(applicantVM);
        }
        public IActionResult Edit(int id)
        {
            var applicant = applicantService.Applicant(id);
            if(applicant != null)
            {
                return View(new EditApplicantVM {
                    ApplicantID = applicant.ApplicantID,
                    Firstname = applicant.Firstname,
                    Lastname = applicant.Lastname,
                    Middlename = applicant.Middlename,
                    DateOfBirth = applicant.DateOfBirth,
                    Religion = applicant.Religion,
                    Nationality = applicant.Nationality,
                });
            }
            ResultMessage = Constants.ApplicantNotFound;
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public IActionResult Edit(EditApplicantVM applicantVM)
        {
            if (ModelState.IsValid)
            {
                var applicant = new Applicant()
                {
                    ApplicantID = applicantVM.ApplicantID,
                    Firstname = applicantVM.Firstname?.ToUpper(),
                    Lastname = applicantVM.Lastname?.ToUpper(),
                    Middlename = applicantVM.Middlename?.ToUpper(),
                    DateOfBirth = applicantVM.DateOfBirth,
                    Religion = applicantVM.Religion,
                    Nationality = applicantVM.Nationality,
                    EmailAddress = applicantVM.EmailAddress
                };

                var result = applicantService.Update(applicant);
                RResult = JsonConvert.SerializeObject(result);

                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(applicantVM);
        }
        public IActionResult Details(int id)
        {
            var applicant = applicantService.ApplicantWithApplications(id);
            if (applicant != null)
            {
                return View(new EditApplicantVM
                {
                    ApplicantID = applicant.ApplicantID,
                    Firstname = applicant.Firstname,
                    Lastname = applicant.Lastname,
                    Middlename = applicant.Middlename,
                    DateOfBirth = applicant.DateOfBirth,
                    Religion = applicant.Religion,
                    Nationality = applicant.Nationality,
                    ApplicationDate = applicant.RegistredDate,
                    Applications = applicant.Applications.ToList()
                });
            }
            ResultMessage = Constants.ApplicantNotFound;
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public IActionResult Delete(int applicantID)
        {
            var result = applicantService.Delete(applicantID);
            RResult = JsonConvert.SerializeObject(result);

            if (result.Succeeded)
            {
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Details),new { id = applicantID });
        }
        [HttpPost]
        public IActionResult ApplyPosition(int applicantID, string positionCode)
        {
            if(positionCode != null)
            {
                var result = applicantService.ApplyPosition(applicantID, positionCode);
                RResult = JsonConvert.SerializeObject(result);
            }
            else
            {
                ResultMessage = Constants.UnexpectedFormError;
            }
            return RedirectToAction(nameof(OpenPositionController.Index),nameof(OpenPositionController).Replace("Controller",""),new { applicantID });
        }
    }
}