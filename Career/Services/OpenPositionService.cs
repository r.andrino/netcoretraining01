﻿using Career.Data;
using Career.Data.Entities;
using Career.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Services
{
    public class OpenPositionService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository<OpenPosition> repository;

        public OpenPositionService(IUnitOfWork unitOfWork, IRepository<OpenPosition> repository)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        public RResult AddOpenPosition(OpenPosition position)
        {
            try
            {
                repository.Add(position);
                unitOfWork.SaveChanges();

                return new RResult(true, "New position has been submitted!");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }
        }
        public IEnumerable<OpenPosition> OpenPositions()
        {
            return repository.Get().Include(x=>x.Applications);
        }
        public OpenPosition OpenPosition(string id)
        {
            return repository.Find(id);
        }
        public RResult Update(OpenPosition position)
        {
            try
            {
                var old = repository.Find(position.PositionCode);
                old.Title = position.Title;
                old.Responsibilities = position.Responsibilities;
                old.TargetApplicants = position.TargetApplicants;

                repository.Update(old);
                unitOfWork.SaveChanges();
                return new RResult(true, "Position's changes has been saved.");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }

        }
        public RResult Delete(string positionCode)
        {
            try
            {
                repository.Delete(positionCode);
                unitOfWork.SaveChanges();
                return new RResult(true, "Position has been deleted.");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }
        }
    }
}
