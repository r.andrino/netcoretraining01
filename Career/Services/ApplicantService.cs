﻿using Career.Data;
using Career.Data.Entities;
using Career.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Services
{
    public class ApplicantService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository<Applicant> applicantRepository;
        private readonly IRepository<Application> applicationRepository;
        private readonly IRepository<OpenPosition> openPositionRepository;

        public ApplicantService(IUnitOfWork unitOfWork, IRepository<Applicant> applicantRepository, IRepository<Application> applicationRepository, IRepository<OpenPosition> openPositionRepository)
        {
            this.unitOfWork = unitOfWork;
            this.applicantRepository = applicantRepository;
            this.applicationRepository = applicationRepository;
            this.openPositionRepository = openPositionRepository;
        }

        public RResult AddApplicant(Applicant applicant)
        {
            try
            {
                applicant.RegistredDate = applicantRepository.GetServerDate();
                applicantRepository.Add(applicant);
                
                unitOfWork.SaveChanges();

                return new RResult(true,"Applicant record has been submitted!");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }
        }
        public IEnumerable<Applicant> Applicants()
        {
            return applicantRepository.Get();
        }
        public Applicant Applicant(int id)
        {
            return applicantRepository.Find(id);
        }
        public RResult Update(Applicant applicant)
        {
            try
            {
                var old = applicantRepository.Find(applicant.ApplicantID);
                old.Firstname = applicant.Firstname;
                old.Lastname = applicant.Lastname;
                old.Middlename = applicant.Middlename;
                old.DateOfBirth = applicant.DateOfBirth;
                old.Religion = applicant.Religion;
                old.Nationality = applicant.Nationality;

                applicantRepository.Update(old);
                unitOfWork.SaveChanges();
                return new RResult(true, "Applicant changes has been saved.");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }

        }
        public RResult Delete(int applicantID)
        {
            try
            {
                applicantRepository.Delete(applicantID);
                unitOfWork.SaveChanges();
                return new RResult(true,"Applicant has been deleted.");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }
        }
        public RResult ApplyPosition(int applicantID, string positionCode)
        {
            try
            {
                var position = openPositionRepository.Find(positionCode);
                if(position != null)
                {
                    int applicantsCount = applicationRepository.Get(x => x.PositionCode == positionCode).Count();
                    var serverdate = applicantRepository.GetServerDate();
                    if(applicantsCount < position.TargetApplicants)
                    {
                        var application = new Application() { ApplicantID = applicantID, PositionCode = positionCode, ApplicationDate = serverdate };
                        applicationRepository.Add(application);
                        unitOfWork.SaveChanges();
                        return new RResult(true, "Position Successfuly Applied.");
                    }
                    return new RResult(false,"Unable to apply position. Position is closed or full!");
                }
                return new RResult(false,"Position not found!");
            }
            catch (Exception e)
            {
                return new RResult(e);
            }
        }
        public bool Applied(int applicantID, string positionCode)
        {
            return applicationRepository.Any(x=>x.ApplicantID == applicantID && x.PositionCode == positionCode);
        }
        public Applicant ApplicantWithApplications(int applicantID)
        {
            return applicantRepository.Get(x => x.ApplicantID == applicantID)
                .Include(x => x.Applications)
                .Include("Applications.OpenPosition").FirstOrDefault();
        }
    }
}
