﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Career.Migrations
{
    public partial class OpenPositionsAndApplicationsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailAddress",
                table: "Applicants",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OpenPositions",
                columns: table => new
                {
                    PositionCode = table.Column<string>(type: "char(10)", maxLength: 10, nullable: false),
                    Title = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Responsibilities = table.Column<string>(nullable: false),
                    TargetApplicants = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenPositions", x => x.PositionCode);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    ApplicationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicantID = table.Column<int>(nullable: false),
                    PositionCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.ApplicationID);
                    table.ForeignKey(
                        name: "FK_Applications_Applicants_ApplicantID",
                        column: x => x.ApplicantID,
                        principalTable: "Applicants",
                        principalColumn: "ApplicantID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Applications_OpenPositions_PositionCode",
                        column: x => x.PositionCode,
                        principalTable: "OpenPositions",
                        principalColumn: "PositionCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Applications_ApplicantID",
                table: "Applications",
                column: "ApplicantID");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_PositionCode",
                table: "Applications",
                column: "PositionCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "OpenPositions");

            migrationBuilder.DropColumn(
                name: "EmailAddress",
                table: "Applicants");
        }
    }
}
