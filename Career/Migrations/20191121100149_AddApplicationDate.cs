﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Career.Migrations
{
    public partial class AddApplicationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationDate",
                table: "Applicants");

            migrationBuilder.AddColumn<DateTime>(
                name: "ApplicationDate",
                table: "Applications",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "RegistredDate",
                table: "Applicants",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationDate",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "RegistredDate",
                table: "Applicants");

            migrationBuilder.AddColumn<DateTime>(
                name: "ApplicationDate",
                table: "Applicants",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
