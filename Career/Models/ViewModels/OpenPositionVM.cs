﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Models.ViewModels
{
    public class OpenPositionVM
    {
        [StringLength(10)]
        public string PositionCode { get; set; }
        [StringLength(50), Required]
        public string Title { get; set; }
        [Required]
        public string Responsibilities { get; set; }
        public short TargetApplicants { get; set; }
    }
}
