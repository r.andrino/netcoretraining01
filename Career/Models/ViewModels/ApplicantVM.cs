﻿using Career.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Career.Models.ViewModels
{
    public class ApplicantVM
    {
        [StringLength(50), Required]
        public string Firstname { get; set; }
        [StringLength(50), Required]
        public string Lastname { get; set; }
        [StringLength(50)]
        public string Middlename { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [StringLength(50)]
        public string Religion { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        [StringLength(50)]
        public string EmailAddress { get; set; }
        public DateTime? ApplicationDate { get; set; }
    }
    public class EditApplicantVM : ApplicantVM
    {
        public int ApplicantID { get; set; }
        public List<Application> Applications { get; set; }
    }
}
