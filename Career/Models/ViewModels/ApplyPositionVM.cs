﻿namespace Career.Models.ViewModels
{
    public class ApplyPositionVM
    {
        public int ApplicantID { get; set; }
        public string PositionCode { get; set; }
    }
}
