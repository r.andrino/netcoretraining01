﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Models;
using IdentityServer.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;

        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleVM model)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole() { Name = model.RoleName };
                var result = await roleManager.CreateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("Roles");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Roles()
        {
            var roles = await roleManager.Roles.ToListAsync();
            return View(roles);
        }

        public async Task<IActionResult> UpdateRole(string id)
        {
            var role = await roleManager.FindByIdAsync(id);
            if(role != null)
            {
                var users = await userManager.GetUsersInRoleAsync(role.Name);
                var roleToEdit = new UpdateRoleVM() { RoleID = role.Id, RoleName = role.Name, Users = users.Select(x=> new ApplicationUser() { Email = x.Email, Id = x.Id }).ToList() };
                return View(roleToEdit);
            }
            return NotFound("Role not found!");
        }
        [HttpPost,Authorize(Policy = "DeleteRolePolicy")]
        public async Task<IActionResult> DeleteRole(string id)
        {
            var role = await roleManager.FindByIdAsync(id);
            if (role != null)
            {
                var result = await roleManager.DeleteAsync(role);
                if (result.Succeeded)
                {
                    return RedirectToAction("Roles");
                }
            }
            return NotFound("Role not found!");
        }
        [HttpPost, Authorize(Roles = "UpdateRolePolicy")]
        public async Task<IActionResult> UpdateRole(UpdateRoleVM model)
        {
            if (ModelState.IsValid)
            {
                var role = await roleManager.FindByIdAsync(model.RoleID);
                role.Name = model.RoleName;
                var result = await roleManager.UpdateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("Roles");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
        }

        public async Task<IActionResult> AddOrRemoveUsersInRole(string id)
        {
            ViewBag.RoleID = id;
            var role = await roleManager.FindByIdAsync(id);
            if(role == null)
            {
                return NotFound("Role not found!");
            }
            var users = userManager.Users;

            var model = new List<UserRoleVM>();
            foreach (var user in users)
            {
                var userRoleVM = new UserRoleVM() { 
                    UserID = user.Id,
                    Username = user.UserName
                };

                userRoleVM.IsSelected = await userManager.IsInRoleAsync(user, role.Name);
                model.Add(userRoleVM);
            }

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AddOrRemoveUsersInRole(string roleID, List<UserRoleVM> model)
        {
            var role = await roleManager.FindByIdAsync(roleID);
            if(role == null)
            {
                return NotFound("Role not found!");
            }

            IdentityResult result = null;
            for(int i = 0; i < model.Count; i++)
            {
                var user = await userManager.FindByIdAsync(model[i].UserID);
                if (model[i].IsSelected && !(await userManager.IsInRoleAsync(user,role.Name)))
                {
                    result = await userManager.AddToRoleAsync(user, role.Name);
                } 
                else if(!model[i].IsSelected && await userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }

                if (result.Succeeded)
                {
                    if(i < (model.Count - 1))
                    {
                        continue;
                    }
                    else
                    {
                        return RedirectToAction("UpdateRole", new { Id = roleID });
                    }
                }
            }

            return RedirectToAction("UpdateRole", new { Id = roleID });
        }

        public async Task<IActionResult> Users()
        {
            var users = await userManager.Users.ToListAsync();
            return View(users);
        }

        public async Task<IActionResult> UpdateUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if(user == null)
            {
                return NotFound("User not found!");
            }

            var userClaims = await userManager.GetClaimsAsync(user);
            var userRoles = await userManager.GetRolesAsync(user);

            var model = new UpdateUserVM() {
                UserID = user.Id,
                Email = user.Email,
                Username = user.UserName,
                IsEmailConfirmed = user.EmailConfirmed,
                //Fullname = user.FullName,
                Claims = userClaims.Select(x => x.Value).ToList(),
                Roles = userRoles.Select(x => x).ToList()
            };



            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateUser(UpdateUserVM model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByIdAsync(model.UserID);
                if (user == null)
                {
                    return NotFound("User not found!");
                }


                user.UserName = model.Username;
                //user.FullName = model.Fullname;
                user.Email = model.Email;
                user.EmailConfirmed = model.IsEmailConfirmed;

                var result = await userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Users");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound("User not found!");
            }

            var result = await userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Users");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View("Users");
        }

        public async Task<IActionResult> AddOrRemoveUserRoles(string id)
        {
            ViewBag.UserID = id;
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound("User not found!");
            }

            var model = new List<UserRolesVM>();

            foreach (var role in roleManager.Roles)
            {
                var userRolesVM = new UserRolesVM() { 
                    RoleID = role.Id,
                    RoleName = role.Name
                };

                userRolesVM.IsSelected = await userManager.IsInRoleAsync(user, role.Name);
                model.Add(userRolesVM);

            }

            return View(model);
        }
        public async Task<IActionResult> AddOrRemoveUserClaims(string id)
        {
            ViewBag.UserID = id;
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound("User not found!");
            }

            var existingUserClaims = await userManager.GetClaimsAsync(user);
            var model = new UserClaimsVM() { UserID = id };

            foreach (Claim claim in ClaimStore.AllClaims)
            {
                UserClaim userClaim = new UserClaim()
                {
                    ClaimType = claim.Type,
                    ClaimValue = claim.Value
                };

                if(existingUserClaims.Any(x=>x.Type == claim.Type))
                {
                    userClaim.IsSelected = true;
                }

                model.Claims.Add(userClaim);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrRemoveUserRoles(string userID, List<UserRolesVM> model)
        {
            var user = await userManager.FindByIdAsync(userID);
            if (user == null)
            {
                return NotFound("User not found!");
            }

            var roles = await userManager.GetRolesAsync(user);
            var result = await userManager.RemoveFromRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }
                
                return View(model);
            }

            result = await userManager.AddToRolesAsync(user,model.Where(x=>x.IsSelected).Select(y=>y.RoleName));
            if (!result.Succeeded)
            {
                ModelState.AddModelError("","Can't add selected roles to user.");
                return View(model);
            }

            return RedirectToAction("UpdateUser",new { id = userID });
        }

        [HttpPost]
        public async Task<IActionResult> AddOrRemoveUserClaims(UserClaimsVM model)
        {
            var user = await userManager.FindByIdAsync(model.UserID);
            if (user == null)
            {
                return NotFound("User not found!");
            }

            var claims = await userManager.GetClaimsAsync(user);
            var result = await userManager.RemoveClaimsAsync(user,claims);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Can't remove existing claims.");
                return View(model);
            }
            var claimsss = model.Claims.Where(x => x.IsSelected).Select(y => new Claim(y.ClaimType, y.ClaimValue));
            result = await userManager.AddClaimsAsync(user,claimsss);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Can't add claims to user.");
                return View(model);
            }

            return RedirectToAction("UpdateUser", new { id = model.UserID });
        }
    }
}