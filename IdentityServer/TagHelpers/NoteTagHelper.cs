﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.TagHelpers
{
    [HtmlTargetElement("note-tag-helper")]
    public class NoteTagHelper : TagHelper
    {
        public string Note { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "NoteTagHelper";
            output.TagMode = TagMode.StartTagAndEndTag;

            var sb = new StringBuilder();
            sb.AppendFormat("<small class='note'>Hi! {0}</small>", this.Note);

            output.PreContent.SetHtmlContent(sb.ToString());
        }
    }
}
