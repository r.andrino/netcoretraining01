﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.ViewModels
{
    public class EmployeeEditVM : EmployeeCreateVM
    {
        public int ID { get; set; }
        public string ExistingPhotoPath { get; set; }
    }
}
