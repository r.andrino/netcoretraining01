﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.ViewModels
{
    public class UserRolesVM
    {
        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }
    public class UserClaimsVM
    {
        public UserClaimsVM()
        {
            Claims = new List<UserClaim>();
        }
        public string UserID { get; set; }
        public List<UserClaim> Claims { get; set; }
    }
    public class UserClaim
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public bool IsSelected { get; set; }
    }
}
