﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.ViewModels
{
    public class UpdateUserVM
    {
        public UpdateUserVM()
        {
            Claims = new List<string>();
            Roles = new List<string>();
        }
        public string UserID { get; set; }
        public string Fullname { get; set; }
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Username { get; set; }
        [Display(Name = "Email Confirmed")]
        public bool IsEmailConfirmed { get; set; }

        public List<string> Claims { get; set; }
        public List<string> Roles { get; set; }
    }
}
