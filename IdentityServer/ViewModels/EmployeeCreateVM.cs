﻿
using IdentityServer.Enums;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace IdentityServer.ViewModels
{
    public class EmployeeCreateVM
    {
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [EmailAddress, Required, MaxLength(50)]
        public string Email { get; set; }
        [Required]
        public DepartmentEnum? Department { get; set; }
        public IFormFile Photo { get; set; }
    }
}
