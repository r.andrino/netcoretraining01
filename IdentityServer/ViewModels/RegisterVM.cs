﻿
using IdentityServer.Filters;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models
{
    public class RegisterVM
    {
        [Required,EmailAddress, Remote(action: "isEmailInUse",controller: "account"), ValidEmailDomain("arcanys.com",ErrorMessage = "Domain must be @arcanys.com")]
        public string Email { get; set; }
        [Required,DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password), Display(Name = "Confirm Password"), Compare("Password",ErrorMessage = "Password and confirm password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required, Display(Name = "Full Name")]
        public string FullName { get; set; }
    }
}
