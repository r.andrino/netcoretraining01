﻿using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.ViewModels
{
    public class CreateRoleVM
    {
        [Required]
        public string RoleName { get; set; }
    }
    public class UpdateRoleVM : CreateRoleVM
    {
        [Required]
        public string RoleID { get; set; }

        public List<ApplicationUser> Users { get; set; }
    }
    public class UserRoleVM
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public bool IsSelected { get; set; }
    }
}
