﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Enums
{
    public enum DepartmentEnum
    {
        None = 1, HR = 2, IT = 3, Payroll = 4
    }
}
